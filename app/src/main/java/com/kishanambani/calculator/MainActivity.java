package com.kishanambani.calculator;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    Button temp;
    TextView tvDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvDisplay = findViewById(R.id.tvActDisplay);
        temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                temp = findViewById(view.getId());
                tvDisplay.setText(temp.getText().toString());
            }
        });
    }
}